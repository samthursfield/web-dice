"use strict";

function dice_initialize(container) {
    var params = $t.get_url_params();

    var notation = params.notation || '1d6';

    var canvas = $t.id('canvas');
    var label = $t.id('label');
    var info_div = $t.id('info_div');

    $t.dice.use_true_random = false;

    if (params.chromakey) {
        $t.dice.desk_color = 0x00ff00;
        info_div.style.display = 'none';
        $t.id('control_panel').style.display = 'none';
    }
    if (params.shadows == 0) {
        $t.dice.use_shadows = false;
    }
    if (params.color == 'white') {
        $t.dice.dice_color = '#808080';
        $t.dice.label_color = '#202020';
    }

    var box = new $t.dice.dice_box(canvas, { w: canvas.clientWidth, h: canvas.clientHeight - 20 });

    $t.bind(window, 'resize', function() {
        box.reinit(canvas, { w: canvas.clientWidth, h: canvas.clientHeight - 20 });
    });

    function before_roll(vectors, notation, callback) {
        // do here rpc call or whatever to get your own result of throw.
        // then callback with array of your result, example:
        // callback([2, 2, 2, 2]); // for 4d6 where all dice values are 2.
        callback();
    }

    function notation_getter() {
        return $t.dice.parse_notation(notation);
    }

    function after_roll(notation, result) {
        // do nothing
    }

    box.bind_mouse(container, notation_getter, before_roll, after_roll);
    box.bind_throw(container, notation_getter, before_roll, after_roll);

    // Roll away!
    $t.raise_event(container, 'mouseup');
}
