Web dice roller

Use this with video chat, screen share + screen annotations to make fun online
board games!

This is intended for deployment using dokku and dokku-letsencrypt, following
instructions here: https://github.com/dokku/dokku-letsencrypt#dockerfile-deploys

See mine at https://webdice.afuera.me.uk/

Based on http://a.teall.info/dice/
Sources from http://www.teall.info/2014/01/online-3d-dice-roller.html
