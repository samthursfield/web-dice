FROM nginx:alpine

COPY ./dice /srv/site/
COPY ./teal.js /srv/site/
COPY ./libs /srv/site/libs

COPY ./config/nginx.conf /etc/nginx/nginx.conf

EXPOSE 5000
